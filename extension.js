// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const schedule = require('node-schedule');
const readline = require('readline');
const fs = require('fs');

let index = 0;
const arr = [];

let index1 = 0;

let splitStr = (str) => {
    if (str.length < 40) {
        var rs = [];
        rs[0] = str;
        return rs
    } else {
        let size = 40
        let count = Math.ceil(str.length / size)
        let rs = []
        for (let i = 0; i <= count; i++) {
            if (i === 0) {
                rs.push(str.substring(i, size))
            } else if (i > 0 && i < count) {
                rs.push(str.substring((i * size), (i + 1) * size))
            } else {
                rs.push(str.substring((i * size), str.length - 1))
            }
        }
        return rs;
    }
}
// 自动定时任务
let sylvia = undefined;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	let read = vscode.commands.registerCommand('readme.read', function () {	
		index = vscode.workspace.getConfiguration().get('ReadMe.sylvia.line');
		var filePath = vscode.workspace.getConfiguration().get('ReadMe.sylvia.path');
		if(filePath===''){
			vscode.window.showInformationMessage('请先设置要阅读的TXT图书');
			return;
		}
		var fRead = fs.createReadStream(filePath, { encoding:'utf8'});
		var objReadline = readline.createInterface({
			input:fRead
		});
		objReadline.on('line',function (line) {
			// arr.push(splitStr(line));
			arr.push(line);
		});
		objReadline.on('close',function () {
			vscode.window.showInformationMessage('read close');
			if(index > arr.length-1 || index < 0){
				index = 0;
			}
			vscode.window.setStatusBarMessage(arr[index]);
		});
		vscode.window.showInformationMessage('read from readme!');
	});
	context.subscriptions.push(read);

	let prePage = vscode.commands.registerCommand('readme.prePage', function () {
		if(arr.length === 0){
			vscode.window.showInformationMessage('please read start first or choose a text not empty');
			return;
		}
		index=index==0?index:index-1;
		let arr1 = splitStr(arr[index]);
		index1 = 0;
		vscode.window.setStatusBarMessage(arr1[index1]);
		index1+=1;

		vscode.workspace.getConfiguration().update('ReadMe.sylvia.line', index, true);
	});
	context.subscriptions.push(prePage);

	function NextPage() {
		if(arr.length === 0){
			vscode.window.showInformationMessage('please read start first or choose a text not empty');
			return;
		}
		let arr1 = splitStr(arr[index]);
		if(index1==arr1.length)
		{
			index+=1;
			arr1 = splitStr(arr[index]);
			index1 = 0;
		}
		vscode.window.setStatusBarMessage(arr1[index1]);
		index1+=1;
		
		vscode.workspace.getConfiguration().update('ReadMe.sylvia.line', index, true);
	};
	let nextPage = vscode.commands.registerCommand('readme.nextPage', function() { NextPage() } );
	context.subscriptions.push(nextPage);

	let close = vscode.commands.registerCommand('readme.close', function () {
		vscode.window.showInformationMessage('close from readme!');
		vscode.window.setStatusBarMessage('');
	});
	context.subscriptions.push(close);

	let autoStart = vscode.commands.registerCommand('readme.autoStart', function () {
		if(arr.length === 0){
			vscode.window.showInformationMessage('please read start first or choose a text not empty');
			return;
		}
		vscode.window.showInformationMessage('auto start');
		const timer = vscode.workspace.getConfiguration().get('ReadMe.sylvia.timer');
		if(timer==undefined || timer>59 || timer<1){
			vscode.window.showInformationMessage('timer set error');
			return;
		}
		sylvia = schedule.scheduleJob('0/'+timer+' * * * * ? ', function() { 
			NextPage();
		});
	});
	context.subscriptions.push(autoStart);

	let autoStop = vscode.commands.registerCommand('readme.autoStop', function () {
		if(sylvia === undefined){
			vscode.window.showInformationMessage('auto read not start');
			return;
		}
		sylvia.cancel();
		vscode.window.showInformationMessage('auto stop');
	});
	context.subscriptions.push(autoStop);

	let setBook = vscode.commands.registerCommand('readme.setBook', function () {
		const options = {
			canSelectMany: false,
			openLabel: 'Open',
			filters: {
			   'Text files': ['txt']
		   }
	   };
	   vscode.window.showOpenDialog(options).then(fileUri => {
		   if (fileUri && fileUri[0]) {
				vscode.workspace.getConfiguration().update('ReadMe.sylvia.line', 0, true);
				vscode.workspace.getConfiguration().update('ReadMe.sylvia.path', fileUri[0].fsPath, true);
				vscode.window.showInformationMessage('text chose');
			}
	   });
	});
	context.subscriptions.push(setBook);
}

function deactivate() {}

module.exports = {
	activate,
	deactivate
}

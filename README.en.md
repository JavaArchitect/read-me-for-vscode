# ReadMe

#### Description
This is a vs code extension that is a txt reader named read me. You can read the txt document in the status bar by shortcut or auto reading after the plug-in installed. We provide the entry of TXT file selection, progress setting, etc. You can learn more in Feature Contributions, and edit them in settings.

#### Software Architecture
- ReadMe
- ├─.gitee
- ├─.vscode
- ├─node_modules
- ├─src
- ├─test
- ├─.eslintrc.json
- ├─.gitignore
- ├─CHANGELOG.md
- ├─extension.js
- ├─jsconfig.json
- ├─package-lock.json
- ├─package.json
- ├─README.en.md
- ├─README.md
- └─vsc-extension-quickstart.md

#### Installation

1.  Open the VsCode
2.  Click the modular named 'Extensions'
3.  Click the three horizontal points, Views and more Actions...
4.  Install from VSIX...

#### Instructions

- readme.read			Alt+R
- readme.nextPage		Alt+N
- readme.prePage		Alt+P
- readme.close		    Alt+C
- readme.autoStart	    Ctrl+Alt+A
- readme.autoStop		Ctrl+Alt+S
- readme.setBook		Ctrl+Alt+B

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
# ReadMe

#### 介绍
这是一个vs代码扩展，是一个名为readme的txt阅读器。安装插件后，可以通过快捷方式或自动读取方式读取状态栏中的txt文档。我们提供TXT文件选择、进度设置等条目。您可以在功能贡献中了解更多信息，并在设置中编辑它们。

#### 软件架构
- ReadMe
- ├─.gitee
- ├─.vscode
- ├─node_modules
- ├─src
- ├─test
- ├─.eslintrc.json
- ├─.gitignore
- ├─CHANGELOG.md
- ├─extension.js
- ├─jsconfig.json
- ├─package-lock.json
- ├─package.json
- ├─README.en.md
- ├─README.md
- └─vsc-extension-quickstart.md

#### 安装教程

1.  打开VsCode
2.  单击名为“扩展”的模块
3.  单击三个水平点、视图和更多操作。。。
4.  从VSIX安装。。。

#### 使用说明

- readme.read			Alt+R
- readme.nextPage		Alt+N
- readme.prePage		Alt+P
- readme.close		    Alt+C
- readme.autoStart	    Ctrl+Alt+A
- readme.autoStop		Ctrl+Alt+S
- readme.setBook		Ctrl+Alt+B

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request